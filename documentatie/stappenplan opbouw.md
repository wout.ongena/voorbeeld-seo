
# Stappenplan opbouw voorbeeld SEO van Wout Ongena

**Hier onder staan enkele van de belangrijkste dingen die je kan doen om je website te optimaliseren voor SEO. Je kan hier heel diep in gaan maar ik heb het bij de in mijn ogen belangrijkste zaken gehouden.
Dit stappenplan neemt een bestaande website als startpunt.**

1. **Maak een robots.txt bestand aan**
   Maak een tekstbestand aan met de naam robots.txt in de root van de map waar de website zich in bevind. Het is belangrijk dat dit in de root staat omdat het anders een zoekmachine het niet kan lezen. Zet hier per groep per zoekmachine wat je wil weergeven en wat niet. In dit project laat ik het niet toe om de test pagina te vinden via google. Voor je eigen website doe je best onderzoek naar welke pagina's je wilt verbergen, dit zal van site tot site afhangen. Ook laat ik het niet toe om in de mappen js en mail te zoeken omdat daar ook niks relevant staat.(https://developers.google.com/search/docs/advanced/robots/create-robots-txt)
   er bestaan ook handige tools om deze pagina voor jou te genereren: https://en.ryte.com/free-tools/robots-txt-generator/#custom
   https://www.seoptimer.com/robots-txt-generator
```
    User-Agent: Googlebot  
	Disallow: /test.html  
	Disallow: /js/  
	Disallow: /mail/
```

2. **Maak titels uniek en rangschik ze logisch**
   Geef je pagina's een unieke naam zodat de zoekmachine weet wat er exact op elke pagina staat en zo ze niet door elkaar kan halen. Ook is het belangrijk om de titels op de pagina zelf (h1, h2, h3, ...) logisch te rangschikken zodat een zoekmachine en gebruiker een duidelijk overzicht heeft over wat er op de pagina zelf staat.
   Zo heb ik in elke titel de naam van de pagina weergegeven samen met de naam van de website zelf. (https://neilpatel.com/blog/html-tags-for-seo/)

3. **Correcte opmaak voor afbeeldingen**
   gebruik img of picture tags binnen html zodat zoekmachines correct afbeeldingen kunnen herkennen. benoem ook de bestandsnaam correct, zo kan een zoekmachine weten over wat de afbeelding gaat. Hier zet je ook best een alt tag bij zodat de zoekmachine weet over wat de afbeelding gaat mocht er een probleem zijn met het laden van het bestand zelf. Ook als je url naar de afbeelding een link is is het handig om alt te gebruiken zodat een zoekmachine toch kan zien over wat de foto gaat. (https://www.bigcommerce.com/ecommerce-answers/what-is-an-alt-tag-and-how-does-it-impact-seo/#:~:text=Definition%3A%20An%20alt%20tag%2C%20also,ecommerce%20store%27s%20search%20engine%20rankings.)

```HTML
<img src="../img/Blackhawk-UH1.jpg" alt="UH-60 blackhawk helicopter flying in formation with UH-1 Huey helicopter">
```

4. **Voeg titel en meta beschrijving toe aan pagina**
   Door een metabeschrijving en titel toe te voegen in de head van je pagina kan je beter en beknopter je resultaat kunnen weergeven als je als zoekresultaat wordt weergegeven. Voorbeeld uit voorbeeld website:
```HTML
<title>Coastline - Home</title>  
<meta name="description" content="Coastline is a PMC based in Belgium. We offer a wide variety of security and transport services.">
```
5. **Voeg een sitemap toe**
   Een sitemap is een map van je website waarop alle toegankelijke pagina's staan. Deze sitemap kan door zoekmachines gebruikt worden om een overzicht te krijgen van wat er allemaal op je website staat en waar die dat kan vinden. Er bestaan tools om deze te genereren: https://www.xml-sitemaps.com, https://www.mysitemapgenerator.com, https://www.sureoak.com/seo-tools/google-xml-sitemap-generator
   Zet nu dit bestand in de root van je website.
   Daarna moet je ook de sitemap aan de robots.txt toevoegen zodat de zoekmachine weet waar deze staat. (https://youbiquity.co.uk/articles/content-hub/how-to-add-your-sitemap-to-your-robots-txt-file/)
   Deze lijn code kan je gewoon onderaan je robots.txt bestand toevoegen. (voor dit voorbeeld ben ik er van uitgegaan dat de url van mn website www.coastlinepmc.com is, ookal bestaat die niet echt)
```
Sitemap: https://www.coastlinepmc.com/sitemap.xml
```

